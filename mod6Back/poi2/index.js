const axios = require('axios')
const papa = require('papaparse')

const URL_2019 = 'https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0401/praias-galegas-con-bandeira-azul-2019/001/descarga-directa-ficheiro.csv';


( async ()  => {

    const target= 'Cangas '
    let parsedData

    try {const datos2019 = await axios.get(URL_2019)

        parsedData = papa.parse(datos2019.data, {header: true})
    
    } catch (e) {

        console.log('Error descargando o procesando el fichero')
        
        return
    }
    
    

    const filteredData = parsedData.data.filter(beach => beach['CONCELLO'] === target)

    console.log(filteredData.length)
})()