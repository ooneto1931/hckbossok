const axios = require('axios')
const Papa = require('papaparse')
const yargs = require('yargs/yargs')

const urls = {
    '2017': 'https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0401/praias-galegas-con-bandeira-azul-2019/001/descarga-directa-ficheiro.csv',
    '2018': 'https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0392/praias-galegas-con-bandeira-azul-2018/001/descarga-directa-ficheiro.csv',
    '2019': 'https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0401/praias-galegas-con-bandeira-azul-2019/001/descarga-directa-ficheiro.csv'
};

const URL_2019 = 'https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0401/praias-galegas-con-bandeira-azul-2019/001/descarga-directa-ficheiro.csv';


(async () => {

    const argv = yargs(process.argv)
        .usage('Usage: $0 <command> [options]')
        .example('$0 -c Vigo -y 2019', 'Compute the  number of beaches with blue flags in 2019 in Vigo')
        .alias('c', 'council')
        .nargs('c', 1)
        .describe('c', 'Concello')
        .alias('y', 'year')
        .nargs('y', 1)
        .describe('y', 'Year')
        .choices('y', [2017, 2018, 2019])
        .demandOption(['c', 'y'])
        .help('h')
        .alias('h', 'help')
        .argv;

    const target = argv.council

    if (urls[argv.y] === undefined){
        console.log('NO hay datos de ese año')
        process.exit()
    }


    let parsedData;
    try {
        const datos2019 = await axios.get(urls[argv.year])
        parsedData = Papa.parse(datos2019.data, { header: true })
    } catch (e) {
        console.log('Error descargando o procesando el fichero')
        return
    }
    const filteredData = parsedData.data.filter(beach => beach['CONCELLO'] === target)

    console.log(`En ${target} hay ${filteredData.length} playas con bandera azul`)
})()
