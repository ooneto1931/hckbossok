const axios = require('axios')
const express = require('express')
const Papa = require('papaparse')

const app = express()

const urls = {
    '2017': 'https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0401/praias-galegas-con-bandeira-azul-2019/001/descarga-directa-ficheiro.csv',
    '2018': 'https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0392/praias-galegas-con-bandeira-azul-2018/001/descarga-directa-ficheiro.csv',
    '2019': 'https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0401/praias-galegas-con-bandeira-azul-2019/001/descarga-directa-ficheiro.csv'
};




app.get('/beaches', async (req, res) => {

    // devuelve las playas del año indicado (obligatorio)
    // y de la provincia indicada (opcional)
    const { year, state } = req.query;


    if (year === undefined) {
        res.status(400).send('Missing mandatory param: year')
        return;
    }

    if (urls[year] === undefined) {
        res.status(404).send('Year not found')
        return;
    }

    let parsedData;
    try {

        

            const datos = await axios.get(urls[year])
            
        
        parsedData = Papa.parse(datos.data, { header: true })
    } catch (e) {
        console.log('Error descargando o procesando el fichero')
        return
    }

    if (state === undefined) {
        res.send(parsedData.data)
    } else {
        const result = parsedData.data.filter(beach => beach['C�DIGO PROVINCIA'] === state)
        res.send(result)
    }

})

app.get('/venue', async (req, res) => {

    const urlVenue = 'https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0305/teatros-auditorios/001/descarga-directa-ficheiro.csv'
    let parsedData;
    try {
        const datos = await axios.get(urlVenue)
        parsedData = Papa.parse(datos.data, { header: true })
    } catch (e) {
        console.log('Error descargando o procesando el fichero')
        res.status(500).send()
    }

    res.send(parsedData)
})


app.listen(3000)
