/*// Variablesconst 

const form = document.getElementById('form')
const list = document.getElementById('list')

console.log(list)

//Listener
const crearElemento = mensaje => {
    console.log(mensaje)
}

const enviaMensaje = e => {

    e.preventDefault()
    let mess = e.target.messageTextarea.value
    crearElemento(mess)    
    mess && crearElemento(mess)
    e.target.messageTextarea.value =''
}

form.addEventListener('submit', enviaMensaje)*/


// Variables
const form = document.getElementById('form')
const list = document.getElementById('list')

// Funciones

const crearElemento = mensaje => {
    const li = document.createElement('li')
    li.textContent = mensaje
    const button = document.createElement('button')
    button.classList = 'delete'
    button.textContent = 'x'
    li.appendChild(button)
    list.appendChild(li)
}

const enviarMensaje = e => {
    e.preventDefault()
    const mensaje = e.target.messageTextarea.value
    mensaje && crearElemento(mensaje)
    e.target.messageTextarea.value = ''
}

const borrarElemento = e => {
    if (e.target.classList.contains('delete')) {
        e.target.parentElement.remove()
    }
}

// Listeners
form.addEventListener('submit', enviarMensaje)
list.addEventListener('click', borrarElemento)