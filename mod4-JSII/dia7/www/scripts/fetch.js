// ------ FETCH -----


//Variables
const txt = document.getElementById('txt')
const json = document.getElementById('json')
const api= document.getElementById('api')
const result = document.querySelector('#result ul')


//Funciones
const loadTxt = () => {
    fetch('/data/datos.txt')
        .then(res=> res.text())
        .then(data=> result.textContent = data)
        .catch(err=> console.error(err.message))
       
}

const loadJson = () => {
    console.log('json')
}



const loadApi = () => {
    console.log('api')
}

//Listeners


txt.addEventListener('click', loadTxt)
//json.addEventListener('clik', loadJson)
//api.addEventListener('clik', loadApi)