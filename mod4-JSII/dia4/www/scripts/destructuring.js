//Destructuring

const familia = ['Rick', 'Morty', 'Beth', 'Jerry', 'Summer']

const [rick, morty, lal] = familia
console.log(rick, morty, lal)

const [, , ,jerry, summer] = familia
console.log(summer)

const [t1, t2, ...tail] = familia

console.log(tail)


//objeto

const rickFucks ={

    nombre: 'Rick',
    especie: 'Humano',
    edad: 70,
    ocupación: 'soldador',


}

//tiene que coincidir el nombre de la var con la clave
const {nombre, edad}= rickFucks

console.log(nombre, edad)

const {bar = 123} = rickFucks
console.log(bar)

console.log(rickFucks)

const {nombre:name} = rickFucks
console.log(rickFucks)

//creación

const arr1 = [1,2,3]
const arr2 = [4,5,6]

const fucionArr = [0, ...arr1, ...arr2, 7]


console.log(fucionArr)


const obj1 = {a:1, b:2}
const obj2 = {c:3, d:4}

const fusionObj = {...obj1, ...obj2, e:5}

console.log(fusionObj)

const obj3={a:'a', b:'b', c: 'c', d: 'd'}

const fusionObjCol = {...obj1, ...obj3}

console.log(fusionObjCol)

console.log(obj1)

const fusionFinal= {...rickFucks, ...obj3}
console.log(fusionFinal)










