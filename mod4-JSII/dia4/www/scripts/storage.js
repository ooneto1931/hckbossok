const miStorage = localStorage

console.log(miStorage)

localStorage.setItem('nombre', 'Obiwan')

sessionStorage.setItem('profesion', 'Jedi')

localStorage.setItem('profesion', 'Jedi')

const nombre = localStorage.getItem('nombre')

console.log(nombre)

localStorage.removeItem('profesion')
sessionStorage.removeItem('profesion')

localStorage.clear()

const datosPersonaje= {'nombre': 'ObiWan', 'profesion': 'Jedi'}

localStorage.setItem('datos', JSON.stringify(datosPersonaje))

const guardado = JSON.parse(localStorage.getItem('datos'))
console.log(guardado)