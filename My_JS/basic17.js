/**
 * Imagina que estás haciendo un sistema de clasificación
 * automática de tweets. Se trata de un sistema que, a partir 
 * del texto de un tweet, asigna automáticamente una serie de etiquetas.
 * 
 * Esta clasificación se realiza de acuerdo a las siguientes reglas:
 *     - si el tweet tiene las palabras: PSOE, PP, Podemos, VOX, CS, entonces
 *       asignará la etiqueta 'POLÍTICA'
 *     - si el tweet tiene algunas de las palabras Javascript, PHP o Python,
 *       la etiqueta será 'PROGRAMACIÓN'
 * 
 * El objetivo de este ejercicio es analizar cada tweet y escribir la etiqueta
 * generada por pantalla.
 * 
 * Nota: al avanzar en el curso podremos elaborar una versión más
 * sofisticada de este ejercicio
 */

tweet1 = 'aprendiendo #javascript en Vigo';
tweet2 = 'empezando el segundo módulo del bootcamp!';
tweet3 = 'en un giro de los acontecimientos VOX y Podemos llegan a un acuerdo';
tweet4 = 'no hay quien entienda php, me paso a Java';
tweet5 = 'deja su militancia en el PSOE para dedicarse a su pasión por la programación en Javascript';

tweet = tweet5.toUpperCase()

tweetPoli = tweet.indexOf('PSOE')!==-1 || tweet.indexOf('PP') !==-1 || tweet.indexOf('VOX') !==-1 || tweet.indexOf('CS')!==-1
tweetProgr= tweet.indexOf('JAVASCRIPT')!==-1 || tweet.indexOf('PHP')!==-1 || tweet.indexOf('PYTHON')!==-1

console.log(`Es de política? ${tweetPoli}`)
console.log(`Es de progracación? ${tweetProgr}`)



