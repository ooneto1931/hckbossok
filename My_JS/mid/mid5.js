/**
 * Operando con más de dos elementos: Arrays
 * 
 * Para prácticar con los arrays, se proponen las siguientes actividades:
 *    - crea un nuevo array cuyos elementos sean los de las posiciones
 *      pares del array original
 * 
 * Nota: num es par si num%0 == 0
 */

values = [2, 4, 6, 8, 10]
output = []

i=0

for (; i<values.length; ){
    if (i%2===0){
        output.push(values[i])
    }
    i++
}


console.log(output)