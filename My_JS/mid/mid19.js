/**
 * Arrays y funciones
 * 
 * Vamos a analizar cómo avanza una enfermedad infecciosa.
 * 
 * Para ello representamos en un array la población de 
 * una determinada zona. Hay un 1 si en esa posición del array
 * la persona está infectada, y un cero si no lo está. Ej.:
 * 
 *    [0,0,0,0,1,0,1,1,0]
 * 
 * Teniendo en cuenta que cada persona contagiada infecta
 * a los dos que están a su lado cada día, ¿cuántos días
 * tarda en infectarse toda la población?
 * 
 * Ejemplo de evolución:
 *    Day 0: [0,0,0,0,1,0,1,1,0]
 *    Day 1: [0,0,0,1,1,1,1,1,1]
 *    Day 2: [0,0,1,1,1,1,1,1,1]
 *    Day 3: [0,1,1,1,1,1,1,1,1] 
 *    Day 4: [1,1,1,1,1,1,1,1,1] 
 * 
 * Nota: ¿cómo copiar un array?
 * No lo hemos dado aún, pero para este ejercicio os puede ser útil saber
 * que para copiar un array no vale con hacer:
 *    variableArray2 = variableArray1
 * 
 * como haríamos con los números. En su lugar haremos lo siguiente:
 *    variableArray2 = [...variableArray1]
 * 
 */





function infect(currentInfected) {

    let dayNext = [...currentInfected]

    for (i = 0; i < dayNext.length; i++) {

        if (currentInfected[i] === 1) {
            if (i === 0) {
                dayNext[i + 1] = 1

            } else if (i === day0.length - 1) {
                dayNext[i - 1] = 1
            } else {
                dayNext[i + 1] = 1
                dayNext[i - 1] = 1
            }


        }





    } return dayNext
}

const day0 = [1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1]

let previousDay = day0
counter = 0

while (previousDay.indexOf(0) !== -1) {

    dayNext[i - 1] = 1
    previousDay = infect(previousDay)
    counter++
   

}
 console.log(counter)


/*for (i = 1; i < day0.length; i++) {

    /*if (day0[i] === 1) {

        dayNext[i + 1] = 1
        dayNext[i - 1] = 1
    }
    if (dayNext.indexOf(0) !== -1) {

        for (j = 1; j < dayNext.length - 1; j++) {
            counter++
            if (dayNext[j] === 1) {
                dayNext[j + 1] = 1
                dayNext[j - 1] = 1

                 }
        }

    }


}*/

