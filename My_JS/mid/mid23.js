/**
 * Estamos programando una página de una inmobiliaria. 
 * 
 * Los inmuebles disponibles se representan como un array de objetos
 * con la siguiente estructura:
 * 
 * {
 *    id: 1
 *    name: 'Piso en el centro',
 *    rooms: 2,
 *    metres: 100,
 *    price: 400
 * }
 *  
 * Escribe una función para filtrar los inmuebles que cumplan una serie
 * de condiciones. Dicha función recibirá el array de inmuebles,  el 
 * número mínimo de habitaciones, el número mínimo de metros y el
 * precio máximo.
 * 
 * Si no desea filtrar por alguna de las condiciones, el valor del parámetro
 * será -1
 * 
 */

let houses = [
    {
        id: 1,
        name: 'Piso en el centro',
        rooms: 2,
        metres: 80,
        price: 100000
    },
    {
        id: 2,
        name: 'Piso calle X',
        rooms: 3,
        metres: 90,
        price: 150000
    },
    {
        id: 3,
        name: 'Casa en la playa',
        rooms: 4,
        metres: 120,
        price: 200000
    },

]

/*const filterHouses = (houses, minRooms, minMetres, maxPrice) => {

    let result = [];

    for (let house of houses) {

        const matchesRooms = house.rooms >= minRooms;
        const MatchesPrice = house.price <= maxPrice || maxPrice === -1;
        const matchesMetres = house.metres >= minMetres;

        if (matchesMetres && MatchesPrice && matchesRooms) {
            result.push(house)
        }
    }

    return result
}*/

/*const filterHouses = (houses, minRooms, minMetres, maxPrice) => {

    const filterRooms = house => house.rooms >= minRooms
    const filterPrice = house => house.price <= maxPrice || maxPrice === -1
    const filterMetres = house => house.metres >= minMetres

    let matchesResult = houses

        .filter(filterRooms)
        .filter(filterPrice)
        .filter(filterMetres)

    return matchesResult

}
*/



// usuario1
precioUsuario1 = 170000;
habitacionesUsuario1 = -1;
metrosUsuario1 = -1;

// usuario2
precioUsuario2 = -1;
habitacionesUsuario2 = 3;
metrosUsuario2 = -1;

resultadoUsuario1 = filterHouses(houses, habitacionesUsuario1, metrosUsuario1, precioUsuario1);   // [{id:1, ...},{id: 2,...}] 
resultadoUsuario2 = filterHouses(houses, habitacionesUsuario2, metrosUsuario2, precioUsuario2);   // [{id:1, ...},{id: 2,...}] 

console.log(resultadoUsuario1);
console.log(resultadoUsuario2);
