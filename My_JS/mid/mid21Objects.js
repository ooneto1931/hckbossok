// Objects
// JSON - javascript object notation

const persona = {
    edad: 20,
    nombre: 'Ana',
    esEstudiante: true,
    notas: [5, 9, 3],
    'numero de hijos': 0
}

const persona2 = {
    edad: 20,
    nombre: 'Manolo',
    esEstudiante: true,
    notas: [5, 9, 3],
    'numero de hijos': 0
}

// accediendo a los campos de una estructura
console.log(persona.nombre)
console.log(persona['nombre'])

// acceder a campos con espacios en blanco
console.log(persona['numero de hijos'])

persona.edad = 21
console.log(persona.edad)

console.log(persona)


// añadir campos dinámicamente
persona.nuevoCampo = 'valor de nuevo campo'
console.log(persona)

// Inicialización de estructura vacía
const miEstructura = {}

// Listando todas las claves del objeto
const keys = Object.keys(persona)
console.log(keys)

for (let key of keys) {
    console.log(`key: ${key}   : ${ persona[key] }`)
}

// Listando todos los valores
const values = Object.values(persona);
console.log(values)

// Borrando una clave de un objeto
delete persona['nuevoCampo']
console.log(persona)


let a = 10
let b = a

b= 15

console.log(`a: ${a}      b: ${b}`)


let objA = {
    x: 100,
    y: 100
}

let objB = objA;

objB.x = 999

console.log(objA, objB)

let student0 = {
    name: 'Pablo',
    grades: [9,3]
}

const addGrade = (student, grade) =>{
    student.grades.push(grade)
}

addGrade(student0, 10)

console.log('Student original: ', student0)


// Clonar objetos
//let objC = Object.assign({}, objA)
let objC = {...objA}

console.log(objC)

objC.y = 3333;

console.log(objA, objC)

let productos = []
let template = {
    id:0,
    name: '',
    price: ''
}

for (let i=0; i<10; i++) {
    let producto = Object.assign({}, template)
    producto.id = i;

    productos.push(producto)
}
console.log(productos)

const constantObject = {
    x: 0,
    y: 9
};

// no da error ,ya que cambia el contenido, no la dirección 
constantObject.x = 100

// la siguiente línea da error de asignación a constante
//constantObject = objA