/**
 * Dado un array de números devuelve un nuevo 
 * array donde cada elemento se haya desplazado 
 * una posición hacia la derecha.
 * 
 * Ejemplo:
 * 
 * Array original: [1,2,3,4]
 * Array resultante: [4,1,2,3]
 * 
 * Nota cómo se trata de un desplazamiento circular,
 * donde el último elemento pasa a ser el primero
 */
values = [1, 2, 3, 4, 5,6,7];
output = [];
n=3

/*values.slice(values[n-1], values.length-n)
console.log(values.slice(values[n-1], values.length-n))*/

firstArray= values.slice(values.length-n, values.length)
secondArray= values.slice(0, values.length-n)
output=[firstArray, secondArray]

/*output=[...firstArray, ...secondArray]*/
/*output= firstArray.concat(secondArray)*/
output= output.flat()
//*outpu=output.flat()*/>>update node
console.log(output)



/*output=output.flat()

console.log(firstArray,secondArray)
console.log(output)*/



/*for (i=values.length-n;i<values.length;i++){

    output.push(values[i])
}

for(i=0;i<values.length-n;i++){
    output.push(values[i])
}
console.log(values.unshift(3))
console.log(values)


*/