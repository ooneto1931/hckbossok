/**
 * Escribe una aplicación que escriba por pantalla
 * la generación a la que pertenece una persona nacida
 * en el año indicado en la variable `birthYear`
 * 
 *   - Z (1995 - actualidad)
 *   - millenials (1981 - 1994)
 *   - generación X (1969 - 1980)
 *   - baby boom (1949 - 1968)
 * 
 */


birthYear = 2000

if (birthYear>=1949 && birthYear<=1968){
    console.log('Pertenece a la G: Baby Boomers')
} else if(birthYear>=1969 && birthYear<=1980){
    console.log('Pertenece a la G: X')
} else if(birthYear>=1981 && birthYear<=1994){
    console.log('Pertenece a la G: Millenials')
} else{
    console.log('Pertenece a la G: Z')
}