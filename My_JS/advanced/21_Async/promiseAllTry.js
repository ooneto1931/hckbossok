// Descargar las tres URLs de las banderas, poniendo una URL mal
// Promise.all, Promise.allSettled

const axios = require('axios');

const URL_2017 = 'https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0380/praias-galegas-con-bandeira-azul-2017/001/descarga-directa-ficheiro.csv';
const URL_2018 = 'https://abeasfsafsadfrtos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0392/praias-galegas-con-bandeira-azul-2018/001/descarga-directa-ficheiro.csv';
const URL_2019 = 'https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0401/praias-galegas-con-bandeira-azul-2019/001/descarga-directa-ficheiro.csv';

(async () => {
    const promesa_2017 = axios.get(URL_2017);
    const promesa_2018 = axios.get(URL_2018);
    const promesa_2019 = axios.get(URL_2019);
    
    try {
        const respuestas = await Promise.allSettled([promesa_2017, promesa_2018, promesa_2019])

        console.log(respuestas[1])
    } catch(e) {
        console.log(e)
    }

})()

