
/**
 * La siguiente URL devuelve una lista de posts: 
 * 
 * https://jsonplaceholder.typicode.com/posts
 * 
 * y esta otra descargaría el detalle de cada post:
 * 
 * https://jsonplaceholder.typicode.com/posts/1
 * 
 * Como son APIs de prueba y gratuitas, la segunda
 * no aporta más información de la que ya hay en la primera, 
 * pero supondremos que sí lo hace.
 * 
 * a) Usa la primera URL para generar un contador con el 
 * número de posts por cada usuario.
 * 
 * b) Genera un objeto con la siguiente estructura, obtiendo
 * el cuerpo del mensaje de la segunda 
 * 
 * [
 *    {
 *       userId: 111,   // usuario1
 *       posts: [
 *           {
 *               title: <title>
 *               body: <body>     // hay que obtenerlo de la segunda petición
 *           },
 *           {
 *               title: <title>
 *               body: <body>
 *           },
 *       ]
 *   },
 *     {
 *       userId: 222,   // usuario2
 *       posts: [
 *           {
 *               title: <title>
 *               body: <body>     // hay que obtenerlo de la segunda petición
 *           },
 *           {
 *               title: <title>
 *               body: <body>
 *           },
 *       ]
 *   }
 *
 *]
 * 
 * 
 */

const axios = require('axios');

const procesaPostsFind = (postsInfo) => {
    let userPosts = [];

    for (let post of postsInfo) {
        // find es como filter, pero devuelve una sola entrada
        const user = userPosts.find( item => item.userId === post.userId);
        const newPost = {
            body: post.body,
            title: post.title
        }

        if (user !== undefined) {
            user.posts.push(newPost)
        } else {
            userPosts.push(
                {
                    userId: post.userId,
                    posts: [newPost]
                }
            )
        }
    }

    return userPosts;

}   

const procesaPostsClassic = (postsInfo) => {
    let userPosts = [];

    for ( let post of postsInfo ) {
        // compruebo si ya tengo un objeto para ese usuario
        // crear una nueva entrada para un nuevo usuario
        // si ya existe el objeto, hacemos push en su array de posts
        // si no existe, lo creamos, inicializando el array de posts
        // al actual

        // FORMA 1
        let found = false;
        for ( let user of userPosts) {
            if (user.userId === post.userId) {
                
                user.posts.push({
                    body: post.body,
                    title: post.title
                })
                found = true;
                break;
            } 
        }

        if (!found) {
            userPosts.push(
                {
                    userId: post.userId,
                    posts: [{
                        body: post.body,
                        title: post.title
                    }]
                }
            )
        }

    }

    return userPosts;
}




( async () => {
    // obtenemos los IDs (vamos a olvidarnos de que en esta primera descarga
    // ya aparece el body)
    const postsListResponse = await axios.get('https://jsonplaceholder.typicode.com/posts')
    const postsList = postsListResponse.data;

    // a partir de cada ID genero la URL para cada descarga individual, de donde obtendremos
    // los datos
    const urls = postsList.map( item => axios.get(`https://jsonplaceholder.typicode.com/posts/${item.id}`))

    const postsInfoResponse = await Promise.all(urls)

    const postsInfo = postsInfoResponse.map( response => response.data)

    let userPosts = procesaPostsFind(postsInfo);



    console.log(userPosts)

})()




