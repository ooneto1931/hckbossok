
const sleep = (seconds) => {
    console.log('..... start')

    return new Promise( (resolve, reject) => {
        setTimeout( () => {
            console.log('.....timeout')
            resolve('dato-de-prueba')
        }, seconds * 1000)    

        console.log('.... end of promise')
    })

    console.log('.....end')
}

(async () => {
    console.log('Antes')
    const sleepVar = await sleep(10)
    console.log(sleepVar)
    console.log('después')
})()
