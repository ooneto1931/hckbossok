/**
 * Busca el ISBN de cinco libros (en Amazon, en Casa del LIbro, etc)
 * y usa la siguiente URL para descargar información sobre el mismo.
 * 
 * https://openlibrary.org/api/books?bibkeys=ISBN:9788401328510&jscmd=data&format=json
 * 
 * Finalmente, obtén una lista con los títulos ordenados por año de publicación
 * 
 */

 // La casa de los espíritus   (9788483462034)
 // Harry Potter La piedra filosofal
 // El señor de los anillos - las dos torres
 // El nombre del viento
// Una habitación propia

 const axios = require('axios');

/*
(async () => {
    const isbns = ['9788483462034',  '9788432201622', '9780747575443' ]
    const urls = isbns.map( isbn => `https://openlibrary.org/api/books?bibkeys=ISBN:${isbn}&jscmd=data&format=json`)
    const downloads = urls.map( url => axios.get(url))
   
    const booksInfo = await Promise.all(downloads)
    const booksInfoData = booksInfo.map( bookInfo => bookInfo.data)

//    const booksInfoDataContent = booksInfoData.map( (bookInfo, i) => bookInfo[`ISBN:${isbns[i]}`])
    const booksInfoDataContent = booksInfoData.map( bookInfo => Object.values(bookInfo)[0])
    
    const booksSorted = booksInfoDataContent.sort( (a, b) => a.publish_date - b.publish_date)
    const booksNames = booksSorted.map( book=> book.title)

    console.log(booksNames)
})()
*/

/*
(async () => {
    const sortByPublishDate = (a, b) => a.publish_date - b.publish_date
    const getBookContent = (bookInfo, i) => bookInfo[`ISBN:${isbns[i]}`]
    const extractDataFromResponse = bookInfo => bookInfo.data;

    const isbns = ['9788483462034',  '9788432201622', '9780747575443', '9781906147877' ]

    const downloads = isbns
        .map( isbn => `https://openlibrary.org/api/books?bibkeys=ISBN:${isbn}&jscmd=data&format=json`)
        .map( url => axios.get(url))
   
    const bookNames = (await Promise.all(downloads))
        .map( extractDataFromResponse )
        .map( getBookContent )
        .filter( book => book !== undefined)
        .sort( sortByPublishDate )
        .map( book=> book.title)
//    const booksInfoDataContent = booksInfoData.map( (bookInfo, i) => bookInfo[`ISBN:${isbns[i]}`])
  
    console.log(bookNames)
})()
*/

(async () => {
    const isbns = ['9788483462034',  '9788432201622', '9780747575443', '9781906147877' ]

    const downloads = isbns
        .map( isbn => `https://openlibrary.org/api/books?bibkeys=ISBN:${isbn}&jscmd=data&format=json`)
        .map( url => axios.get(url))
  
          
    const responses = await Promise.all(downloads)

    let books = [];
    for (let response of responses) {
        let bookData = response.data;

        if (Object.keys(bookData).length !== 0) {
            books.push(Object.values(bookData)[0]);
        }
    }

    books = books.sort( (a, b) => a.publish_date - b.publish_date)

    let titles = [];
    for (let book of books ) {
        titles.push(book.title)
    }
    console.log(titles)

        //    const booksInfoDataContent = booksInfoData.map( (bookInfo, i) => bookInfo[`ISBN:${isbns[i]}`])
  
})()
