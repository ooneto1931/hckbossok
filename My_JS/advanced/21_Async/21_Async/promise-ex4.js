/** 
 * Lee el fihero nacional_covid19.csv y crea funciones para lo siguiente
 * (Fuente: https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/nacional_covid19.csv    )
 * 
 * 1) buscar el día en el que hubo mayor incremento en el número de contagios
 * 
 * 2) generar un nuevo array con el porcentaje de nuevos contagios respecto al total de contagios del 
 * día anterior. Ejemplo. Si el día 1 hay 1000 contagios, y el día 2 hay 1200, el incremento ha sido del
 * 20% ((1200 - 1000) / 1000)*100
 * 
 * 3) escribir en pantalla un resumen para el día indicado en los parámetros de la función:
 *         printSummary(day, month, year)
 * 
 * debe pintar en pantalla
 *       ******************************
 *       * Contagios totales: xx      *
 *       * Altas totales: xx          *  
 *       * Incremento contagios: 20%  *
 *       * Fallecidos: xx             *
 *       * UCI: xx                    *
 *       * Hospitalizados: xx         * 
 *       ******************************
 * 
 * Si no existe algunos de los datos, no debe aparecer la línea correspondiente
 * 
 * 
 * 4) calcular cuántos nuevos contagios hay en cada día de la semana. La salida de la función 
 * será un objeto con las claves 'lunes', 'martes',etc. y los valores serán el número
 * de nuevos contagios que ha habido en dichos días
 * 
 * 5) El número de nuevos contagios puede variar mucho de un día a otro por diversos motivos. Para
 * que la interpretación de las gráficas sea más sencilla se suelen suavizar los datos. ¿Cómo? Una de
 * las técnicas es hacer una media de los últimos días. El objetivo de este apartado es crear un array
 * con los nuevos contagios "suavizados" con la media de los últimos tres días. Es decir, el día 3 contendrá
 * la suma de los contagios de los días 1, 2 y 3 dividida por 3.
 * 
 */


 const axios = require('axios');

 const URL = 'https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/nacional_covid19.csv';
/*
 ( async () => {
    const data = (await axios.get(URL)).data;

    // extraigo las líneas
    const lines = data.split('\n')
    
    // de cada línea obtenemos los campos por separado
    const linesWithFields = lines.map( line => line.split(','))

    // nos quedamos con la segunda columna, que es la que tiene
    // el número total de contagios hasta ese día
    const dailyTotal = linesWithFields.map( fields => fields[2])
    
    // generamos un nuevo array donde cada entrada es la resta de 
    // valores consecutivos. Empezamos en el dos, para evitar la 
    // cabecera del CSV y la primera línea, ya que no podemos
    // restarle nada
    let increases=[];
    for (let i=2; i<dailyTotal.length; i++) {
        increases.push( parseInt(dailyTotal[i]) - parseInt(dailyTotal[i-1]))
    }

    console.log(increases)
 })()

 */


 
// [5, 0, 20, 5]

/*const incrementos = totales
    .map( (total, i) => total - totales[i-1]  )
    .slice(1)
*/

const printSummary = ( year, month, day) => {

}

( async () => {
    const data = (await axios.get(URL)).data;

    // extraigo las líneas
    const lines = data.split('\n')
    
    // de cada línea obtenemos los campos por separado
    const linesWithFields = lines.map( line => line.split(','))

    const year = 2020;
    const month = 5;
    const day = 22;
  
    let dayStr, monthStr;

    const targetDate = new Date(year, month, day);

    if (month < 10) {
        monthStr = `0${month}`
    } else {
        monthStr = `${month}`
    }

    if (day <10) {
        dayStr = `0${day}`
    } else {
        dayStr = `${day}`
    }
    const targetDateString = `${year}-${monthStr}-${dayStr}`

/*    const line = linesWithFields.find( fields => {
        //const dateFields = fields[0].split('-');
        const [yearLoop, monthLoop, dayLoop] = fields[0].split('-');

        const dateLoop = new Date(yearLoop, parseInt(monthLoop) -1, dayLoop);

        return dateLoop.getTime() === targetDate.getTime()
    })
*/

//    const line = linesWithFields.find( fields => fields[0] === targetDateString)
//    const line = linesWithFields.findIndex( fields => fields[0] === targetDateString)
//    const line = linesWithFields.filter( fields => fields[0] === targetDateString)


//    console.log(line)


})()