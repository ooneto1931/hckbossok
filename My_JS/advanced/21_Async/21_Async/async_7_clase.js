const axios = require('axios');


const getFlagsForCouncil = (flags, council) => {

    const beaches = flags
        .slice(1)             // elimino cabecera
        .map(line => line.split(';'))     // convierto cada línea en un array
        .filter(campos => campos.length > 1)    // me quedo con los que tengan datos (líneas no vacías)
        .filter(campos => campos[2].toLowerCase() === council.toLowerCase())  // me quedo con los del ayuntamiento que me piden

    return beaches.length;
}


// como usamos await en el interior es necesario indicar que la función es asíncrona
// con la palabra async
const download = async () => {

    // Ejecutamos las tres promesas de manera secuencial
    // Al insertar el `await`, la ejecución esperará a que la Promesa se cumpla (es decir, termine la descarga)
    const datos2017 = await axios.get('https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0380/praias-galegas-con-bandeira-azul-2017/001/descarga-directa-ficheiro.csv')
    const datos2018 = await axios.get('https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0392/praias-galegas-con-bandeira-azul-2018/001/descarga-directa-ficheiro.csv')
    const datos2019 = await axios.get('https://abertos.xunta.gal/catalogo/cultura-ocio-deporte/-/dataset/0401/praias-galegas-con-bandeira-azul-2019/001/descarga-directa-ficheiro.csv')

    return [datos2017.data, datos2018.data, datos2019.data]
}

// como la función download es asíncrona, es decir, como devuelve una promesa
// no podemos llamarla normalmente. Es necesario usar el `.then` para indicarle
// el callback a ejecutar cuando los datos estén listos
download().then( (datos) => {
    console.log(datos)
})

// ojo! esta línea se ejecuta antes de que termine la descarga
console.log('Última línea')
