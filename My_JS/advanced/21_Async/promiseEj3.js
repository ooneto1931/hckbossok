/**
 * Busca el ISBN de cinco libros (en Amazon, en Casa del LIbro, etc)
 * y usa la siguiente URL para descargar información sobre el mismo.
 * 
 * https://openlibrary.org/api/books?bibkeys=ISBN:9788401328510&jscmd=data&format=json
 * 
 * Finalmente, obtén una lista con los títulos ordenados por año de publicación
 * 
 */


// La casa de los espíritus   (9788483462034)
// Harry Potter La piedra filosofal 9781524721251
// El señor de los anillos - las dos torres 9780007141302
// El nombre del viento 9781423389262
// Una habitación propia 9781906147877

const axios = require('axios');


(async () => {

    const isbns = ['9788483462034', '9781524721251', '9780007141302', '9781423389262', '9781906147877']
    const URLBeta = 'https://openlibrary.org/api/books?bibkeys=ISBN:&jscmd=data&format=json'
    let isbnsGet = isbns.map(iter => {
        const URL = `https://openlibrary.org/api/books?bibkeys=ISBN:${iter}&jscmd=data&format=json`
        return axios.get(URL)
    })
    const nuevaLista = await Promise.all(isbnsGet)
    let arrayData =[]
    
    for ( let nuevaListaOk of nuevaLista){
        arrayData.push(nuevaListaOk.data)
       
    }
    //console.log(arrayData)

    let arrayDates =[]

    for (let arrayDatas of arrayData) {
        arrayDates.push(Object.values(arrayDatas))
        
    }
    console.log(arrayDates[2][0].publish_date)

    

    




    


    //console.log(isbnsGet[1])
    /*const URL = await axios.get(`https://openlibrary.org/api/books?bibkeys=ISBN:9788401328510&jscmd=data&format=json`)

    console.log(URL)*/



})()

