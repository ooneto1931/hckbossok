/**
 * Fechas
 * 
 * Javascript incluye facilidades para trabajar con fechas. `Date`
 * es un objeto que ofrece una serie de métodos para manejar fechas.
 * 
 * `new Date()` crea un nuevo objeto en el cual podemos invocar sus 
 * métodos nativos como:
 *    - `getFullYear`
 *    - `getDate()`
 *    - `getMonth()`
 * 
 * Las lista completa puede consultarse en:
 * 
 * https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Date
 * 
 * En el siguiente ejercicio, escribe una función que devuelva 
 * una cadena de texto con un formato con el del siguiente ejemplo:
 *  
 * `Sábado, 20 de abril de 1990`
 * 
 * Para ello, la función recibirá día, mes y año en formato numérico.
 * 
 * Ten en cuenta que:
 *   - para el objeto `Date` los meses empiezan en 0
 *   - quizá no sea necesario para este ejercicio, pero recuerda que 
 *     la hora que devuelve Date cuando la pintamos en pantalla es la
 *     hora universal, sin aplicar la diferencia horaria de cada región
 *     geográfica del mundo.  
 * 
 */


 




 const fechaEscrita = (dia, mes, año) => {
    const fecha= new Date(año, mes-1, dia)

    const diaSemana = fecha.getDay()
    const monthNr = fecha.getMonth()

    let diaSemanaWord = {
        0: 'Domingo',
        1: 'Lunes',
        2: 'Martes',
        3: 'Miercoles',
        4: 'Jueves',
        5: 'Viernes',
        6: 'Sábado',

    }
    let monthName ={
        0: 'enero',
        1: 'febrero',
        2: 'marzo',
        3: 'abril',
        4: 'mayo',
        5: 'junio',
        6: 'julio',
        7: 'agosto',
        8: 'septiembre',
        9: 'octubre',
        10: 'noviembre',
        11: 'diciembre',

    }
    let fechaFinal=`${diaSemanaWord[diaSemana]}, ${dia} de ${monthName[monthNr]} de ${año}`

    return fechaFinal
  
}

    
const fechaNueva= fechaEscrita(20, 10, 1978 )
console.log(fechaNueva)
 /*const dia =25
 const mes = 10
 const año= 2020
 const fecha=  new Date(año, mes, dia)

 const diaSemana = fecha.getDay()
 console.log(diaSemana)*/
