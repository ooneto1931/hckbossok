/**
 * ¿Cuáles son los `hastags` del tweet? 
 * 
 * Escribe el código necesario para extraer de las variables indicadas
 * los `hashtags` automáticamente.
 *   - asume que el texto del `hashtag` tiene 10 caracteres
 *   - asume que hay, como mucho, dos hashtags 
 * 
 * Para ello se propone el uso de las funciones de Javascript `indexOf` y `slice`:
 *   - `indexOf` devuelve la posición en la que se encuentra la cadena buscada
 *   - `slice` extrae un fragmento de una cadena
 * 
 * Ejemplos:
 * 
 *    name = 'Manolo Abreu'
 *    name.indexOf('Abreu')     // devuelve 7
 *    name.indexOf('Martínez')  // devuelve -1
 * 
 * Consultar:
 *    - https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/indexOf
 */


tweet1 = 'aprendiendo #javascript en  Vigo';
tweet2 = 'empezando el segundo módulo del bootcamp!'
tweet3 = 'hack a boss bootcamp vigo #javascript hola #codinglive';

DEFAULT_HASHTAG_LENGTH = 10;

positionT1= tweet3.indexOf('#')
hashT1= tweet3.slice(positionT1, positionT1+11)
hashT2Pos= tweet3.indexOf('#', positionT1 +1 )
hashT2=tweet3.slice(hashT2Pos, hashT2Pos+11)


if (positionT1==-1){
    console.log('No#')
} else {console.log(hashT1, hashT2)}


