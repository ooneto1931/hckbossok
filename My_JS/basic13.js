/**
 * El objetivo de este ejercicio es indicar, con letra, la nota media
 * de un estudiante a partir de sus tres notas parciales. Los intervalos
 * serán los siguientes:
 * 
 *    - de 0 a 4.9, SUSPENSO
 *    - de 5 a 5.9, APROBADO
 *    - de 6 a 6.9, BIEN
 *    - de 7 a 8.9, NOTABLE
 *    - de 9 a 10, SOBRESALIENTE
 */

grade1 = 8;
grade2 = 9;
grade3 = 10;

//avg = (grade1 + grade2 + grade3) / 3;
notaMedia= (grade1+grade2+grade3)/3

if (notaMedia<=4.9){
   console.log('Suspenso')    
} else if (notaMedia<6){
   console.log('Aprobado')
} else if(notaMedia<7){
   console.log('Bien')
} else if(notaMedia<9){
   console.log('Notable')
} else {
   console.log('Sobresaliente')
}
