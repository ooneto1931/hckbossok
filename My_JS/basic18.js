/**
 * Repaso
 * 
 * En este ejercicio hará falta usar todo lo aprendido hasta ahora
 * El objetivo es escribir en pantalla una ficha de estudiante
 * con el siguiente aspecto:
 * 
 *     **********************
 *     * Manolo Pérez Gómez *
 *     *                    *
 *     *        8.3         *
 *     *     (APROBADO)     *
 *     **********************
 * 
 * Notas:
 *    - notad que tanto la nota numérica como textual están centradas
 *    - vuestro código debe funcionar para cualquier nombre y apellidos,
 *      independientemente de su longitud
 *    - podéis hacer uso de la función `repeat`: 
 *      https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/String/repeat
 */   

name = 'anal';
surname1 = 'Pérez';
surname2 = 'Gómez';

grade1 = 10;
grade2 = 5;

fullName=`${name} ${surname1} ${surname2}`

firstLine='*'.repeat(fullName.length+4)
thirdLine=' '.repeat(fullName.length+2)
avg=(grade1 + grade2)/2


if (avg>=5){
    gradeText='APROBADO'
} else {
    gradeText='SUSPENSO'
}
avgFixed=avg.toFixed(2)
halfNameGrade=' '.repeat((fullName.length+2)/2-avgFixed.length/2)
halfGradeSuffix=' '.repeat(fullName.length+2 - halfNameGrade.length - avgFixed.length)

/*fifthLine=' '.repeat((fullName.length/2)-5)
console.log(fullName, gradePrefixLength.length, avgFixed.length)
console.log(gradeSuffixLength.toString().length)
console.log(test.toString().length)*/


console.log(`${firstLine}\n* ${fullName} *\n*${thirdLine}*\n*${halfNameGrade}${avgFixed}${halfGradeSuffix}*`)

console.log(avgFixed)
/*console.log(halfName)*/
