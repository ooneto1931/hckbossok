/**
 * El objetivo de este ejercicio es hacer una calculadora, 
 * súper simplificada, del IRPF. El IRPF es un impuesto que 
 * pagamos al Estado proporcional a nuestros ingresos. Además, 
 * se aplica por tramos, como se indica a continuación:
 *  
 * Tramos IRPF 2019
 *   Desde 0 hasta 12450€: 19%              
 *   De 12450€ a 20200€: 24%
 *   De 20200€ a 35200€: 30%
 *   De 35200€ a 60000€: 37%
 *   Más de 60000€: 45%
 * 
 * Así, una persona que haya ganado 10000 euros, pagaría el 19% de esos 10000. 
 * Si hubiese ganado 15000 euros, pagaría el 19% de los 12450 primeros euros, 
 * y el 24% de la diferencia entre 15000 y 12450.
 * 
 * Disclaimer: este es un cálculo súper simplicado, la fórmula real incluye 
 * muchísimas variables
 * 
 */

salarioBruto = 30000;
seccion1=12450
seccion2=20200
seccion3=35200
seccion4=60000

irpf1= salarioBruto*0.19
irpf2= ((salarioBruto-seccion1)*0.24)+seccion1*0.19
irpf3= seccion1*0.19+(seccion2-seccion1)*0.24+((salarioBruto-(seccion2))*0.3)
irpf4= seccion1*0.19+(seccion2-seccion1)*0.24+(seccion3-seccion2)*0.3+((salarioBruto-(seccion3))*0.37)
irpf5= seccion1*0.19+(seccion2-seccion1)*0.24+(seccion3-seccion2)*0.3+(seccion4-seccion3)*0.37+((salarioBruto-(seccion4))*0.45)

retencion=0

if (salarioBruto<=seccion1){
    retencion=irpf1
} else if (salarioBruto<=seccion2){
    retencion=irpf2
} else if (salarioBruto<=seccion3){
    retencion=irpf3
} else if (salarioBruto<=seccion4){
    retencion=irpf4
} else {retencion=irpf5}

console.log(`La retención aplicada a un salario de $${salarioBruto} es $${retencion}`)