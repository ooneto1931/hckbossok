/**
 * Repaso
 * 
 * En este ejercicio hará falta usar todo lo aprendido hasta ahora
 * El objetivo es escribir en pantalla una ficha de estudiante
 * con el siguiente aspecto:
 * 
 *     **********************
 *     * Manolo Pérez Gómez *
 *     *                    *
 *     *        8.3         *
 *     *     (APROBADO)     *
 *     **********************
 * 
 * Notas:
 *    - notad que tanto la nota numérica como textual están centradas
 *    - vuestro código debe funcionar para cualquier nombre y apellidos,
 *      independientemente de su longitud
 *    - toFixed(2)
 *    - podéis hacer uso de la función `repeat`: 
 *      https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/String/repeat
 */

const name = 'Silvano';
const surname1 = 'P';
const surname2 = 'Gómez';

const grade1 = 7;
const grade2 = 10;

const avgGrade = (grade1 + grade2) / 2
const avgGradeFixed = avgGrade.toFixed(2)


if (avgGrade >=5 ) {
     gradeText = '(APROBADO)'
} else {
    
    gradeText = '(SUSPENSO)'
}

const fullName =`${name} ${surname1} ${surname2}`



function printCenteredText(text, linelength){

    const prefix = ' '.repeat((linelength)/2 - text.length / 2)
    const suffix = ' '.repeat(linelength  - prefix.length - text.length)
    
    

    return `*${prefix}${text}${suffix}*`

}
const lineLength = fullName.length +2+50
/*let printIt = printCenteredText(text, lineLength)*/

console.log('*'.repeat(lineLength+2))
console.log(`${printCenteredText(fullName, lineLength)}`)
console.log(`${printCenteredText('', lineLength, '')}`)
console.log(`${printCenteredText(avgGradeFixed, lineLength)}`)
console.log(`${printCenteredText(gradeText, lineLength)}`)
console.log('*'.repeat(lineLength+2))

test1=[1,2,3,4,5]
test2=[6,7,8,9]

test3=[test1,test2]



test4=[...test1, ...test2]

console.log(test4)




