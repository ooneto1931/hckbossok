/**
 * ¿Cuáles son los `hastags` del tweet? 
 * 
 * Escribe el código necesario para extraer de las variables indicadas
 * los `hashtags` automáticamente.
 *   - asume que el texto del `hashtag` tiene 10 caracteres
 *   - asume que hay, como mucho, dos hashtags 
 * 
 * Para ello se propone el uso de las funciones de Javascript `indexOf` y `slice`:
 *   - `indexOf` devuelve la posición en la que se encuentra la cadena buscada
 *   - `slice` extrae un fragmento de una cadena
 * 
 * Ejemplos:
 * 
 *    name = 'Manolo Abreu'
 *    name.indexOf('Abreu')     // devuelve 7
 *    name.indexOf('Martínez')  // devuelve -1
 * 
 * Consultar:
 *    - https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/indexOf
 */



tweet1 = 'aprendiendo #javascript';
tweet2 = 'empezando el segundo módulo del bootcamp!'
tweet3 = 'hack a boss bootcamp vigo #javascript hola #codinglive';


tweets = [ tweet1, tweet2, tweet3 ]
/*tweets = [
    'aprendiendo #javascript',
    'empezando el segundo módulo del bootcamp!',
    'hack a boss bootcamp vigo #javascript hola #codinglive'
]*/

for (tweet of tweets) {
    // divido la frase en palabras, diviéndola por espacios en blanco
    words = tweet.split(' ')

    hashtags = []

    for (word of words) {
        if (word[0] === '#') {
            hashtags.push(word)
        }
    }

    console.log(hashtags)

}
